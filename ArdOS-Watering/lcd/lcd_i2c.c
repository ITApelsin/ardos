/*
 * lcd_i2c.c
 *
 * Created: 04.12.2019 16:48:30
 *  Author: ITApelsin
 */ 
#include "lcd_i2c.h"
#include <kernel/i2c.h>
#include <util/delay.h>

unsigned char portlcd = 0;

uint8_t lcd_address = 0;

#define setE1() i2c_sendByteTo(portlcd|=0x04, lcd_address)
#define setE0() i2c_sendByteTo(portlcd&=~0x04, lcd_address)
#define setRS1() i2c_sendByteTo(portlcd|=0x01, lcd_address)
#define setRS0() i2c_sendByteTo(portlcd&=~0x01, lcd_address)
#define setwrite() i2c_sendByteTo(portlcd&=~0x02, lcd_address)

void sendhalfbyte(unsigned char c)
{
	c<<=4;
	setE1(); 
	_delay_us(50);
	i2c_sendByteTo(portlcd|c, lcd_address);
	setE0(); 
	_delay_us(50);
}

void sendbyte(unsigned char c, unsigned char mode)
{
	if (mode==0) setRS0(); else setRS1();
	unsigned char hc=0;
	hc=c>>4;
	sendhalfbyte(hc); sendhalfbyte(c);
}

void lcd_init(uint8_t address)
{
	lcd_address = address;
	_delay_ms(15);
	sendhalfbyte(0b00000011);
	_delay_ms(4);
	sendhalfbyte(0b00000011);
	_delay_us(100);
	sendhalfbyte(0b00000011);
	_delay_ms(1);
	sendhalfbyte(0b00000010);
	_delay_ms(1);
	sendbyte(0b00101000, 0);
	_delay_ms(1);
	sendbyte(0b00001100, 0);
	_delay_ms(1);
	sendbyte(0b00000110, 0); 
	_delay_ms(1);
	lcd_clear();
	setwrite();
}

void lcd_gotoXY(uint8_t x, uint8_t y)
{
	switch(y)
	{
		case 0:
			sendbyte(x|0x80, 0);
			break;
		case 1:
			sendbyte((0x40+x)|0x80, 0);
			break;
	}
}

void lcd_clear()
{
	sendbyte(0b00000001, 0);
	_delay_us(1500);
}

void lcd_print_char(char c)
{
	sendbyte(c, 1);
}

void lcd_print_str(char* str)
{
	for (uint8_t i = 0; str[i] != '\0'; i++) 
	{
		lcd_print_char(str[i]);
	}
}

void lcd_print_int(int i)
{
	char buff[16];
	char sign = 0;
	char*p = buff;
	if (i < 0) {
		sign = 1;
		p++;
		i *= -1;
	}
	int shifter = i;
	do 
	{
		p++;
		shifter /= 10;
	} while(shifter);
	*p = '\0';
	do 
	{
		*--p = '0' + i%10;
		i /= 10;
	}while(i);
	if (sign) 
	{
		*--p = '-';
	}
	lcd_print_str(buff);
}

void lcd_enable_backlight()
{
	i2c_sendByteTo(portlcd|=0x08, lcd_address);
}

void lcd_disable_backlight()
{
	i2c_sendByteTo(portlcd&=~0x08, lcd_address);
}