/*
 * lcd_i2c.h
 *
 * Created: 04.12.2019 16:44:02
 *  Author: ITApelsin
 */ 


#ifndef LCD_I2C_H_
#define LCD_I2C_H_

#include <stdint.h>

void lcd_init(uint8_t address);
void lcd_gotoXY(uint8_t x, uint8_t y);
void lcd_clear();
void lcd_print_str(char* str);
void lcd_print_char(char c);
void lcd_print_int(int i);
void lcd_enable_backlight();
void lcd_disable_backlight();

#endif /* LCD_I2C_H_ */