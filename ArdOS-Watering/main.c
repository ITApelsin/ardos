/*
 * ArdOS-GSM.c
 *
 * Created: 02.12.2019 2:26:38
 * Author : ITApelsin
 */ 

#include <ardos.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "lcd/lcd_i2c.h"

#define pump_on() _ardos_pin_high(PORTD, PORTD7)
#define pump_off() _ardos_pin_low(PORTD, PORTD7)

volatile int waterLevel = 0;
volatile int minLevel = 0;

int powerBlinkTask_func(void *args) 
{
	_ardos_pin_xor(PORTB, PB5);
	return 0;
}

int waterSensorMonitor_func(void *args)
{
	waterLevel = analogRead(A0);
	if (waterLevel < minLevel) 
	{
		pump_on();
	} else {
		pump_off();
	}
	return 0;
}

int controlTask_func(void* args) 
{
	if (digitalRead(PIND, PIND3)) 
	{
		minLevel = (minLevel+10)%1024;
		_ardos_pin_high(PORTB, PB3);
		_delay_ms(150);
		_ardos_pin_low(PORTB, PB3);
	}
	if (digitalRead(PIND, PIND4)) 
	{
		pump_on();
		for (int i = 0; i < 20; i ++) 
		{
			_ardos_pin_xor(PORTB, PB3);
			_delay_ms(100);
		}
		pump_off();
	}
	return 0;
}

int updateSceenTask_func(void *args) 
{
	sei();
	while(1)
	{
		ATOMIC_BLOCK(ATOMIC_FORCEON) 
		{
			lcd_clear();
			lcd_gotoXY(0,0);
			lcd_print_str("Water:");
			lcd_print_int(waterLevel);
			lcd_gotoXY(0, 1);
			lcd_print_str("Min:");
			lcd_print_int(minLevel);
			lcd_gotoXY(11,1);
			lcd_print_str("ArdOS");
		}
		_delay_ms(500);
	}
	cli();
	return 0;
}

void onInit()
{
	
	_ardos_pin_output(DDRB, DDB5); // power blink pin output
	_ardos_pin_output(DDRB, DDB3); // io blink pin output
	_ardos_pin_output(DDRD, DDD7); // pump relay pin output
	
	_ardos_pin_low(PORTB, PB5); // reset power pin
	_ardos_pin_low(PORTB, PB3); // reset io pin
	_ardos_pin_low(PORTD, PORTD7); // reset pump relay pin
	
	lcd_init(0x3F);
	lcd_enable_backlight();
	pH* updateSceenProcess = ardos_create_process(updateSceenTask_func, LOW, 20);
	ardos_run_process(updateSceenProcess, 0);
	
	ardos_schedulle_task(powerBlinkTask_func, CRITICAL, 0, 1000, 1); // PowerBlink periodic task
	//ardos_schedulle_task(updateSceenTask_func, HIGHT, 20, 0, 0); // UpdateSreen task
	ardos_schedulle_task(waterSensorMonitor_func, HIGHT, 10, 100, 1); // WaterSensorMonitor periodic task
	ardos_schedulle_task(controlTask_func, HIGHT, 10, 10, 1);
}

int main(void)
{
	ardos_init(onInit);
}

