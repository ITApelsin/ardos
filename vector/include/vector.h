#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>

struct vector_t
{
	void ** volatile data;
	int size;
	int capasity;
};

typedef struct vector_t vector;

/**
*   return 0   if v1 == v2
*   return <0  if v1 > v2
*   return >0  if v1 < v2
**/
typedef signed char (*comparator_func)(void* v1, void* v2);

typedef enum {MIN_TO_MAX, MAX_TO_MIN} sort_type;

// create new vector with max size = size
vector* vector_create(int size);

// free alocated memory
void vector_free(vector* vec);

// return vector max size
int vector_size(vector* vec);

// return current capasity of vector
int vector_capasity(vector *vec);

// push value to vector back
int vector_push(vector* vec, void* value);

// remove and return last element
void* vector_pop_back(vector* vec);

// remove and return first element
void* vector_pop_front(vector* vec);

// remove and return elemtn at position
void* vector_remove(vector* vec, int i);

// return element at position i
void* vector_get(vector* vec, int i);

// return first element
void* vector_first(vector* vec);

// return last element
void* vector_last(vector* vec);

void vector_sort(vector* vec, comparator_func comparator, sort_type st);

#endif // VECTOR_H
