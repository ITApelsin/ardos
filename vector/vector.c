#include "include/vector.h"

vector* vector_create(int size)
{
    vector *vec = (vector*) malloc(sizeof(vector));
    if (!vec) {
        return 0;
    }
    vec->data = malloc(sizeof(void*) * size);
    vec->size = size;
    vec->capasity = 0;
    return vec;
}

void vector_free(vector* vec)
{
    if (vec) {
        if (vec->data) {
            for (int i = 0; i < vec->capasity; i++) {
                free(vec->data[i]);
            }
            free(vec->data);
        }
        free(vec);
    }
}

int vector_size(vector* vec)
{
    if (vec) {
        return vec->capasity;
    }
    return -1;
}

int vector_capasity(vector *vec)
{
    if (vec) {
        return vec->capasity;
    }
    return -1;
}

int vector_push(vector* vec, void* value)
{
    if (!vec || !value || vec->capasity >= vec->size) {
        return -1;
    }
    vec->data[vec->capasity] = value;
    return vec->capasity++;
}

void* vector_remove(vector* vec, int i)
{
    /*if (!vec || i < 0 || i > vec->capasity - 1) {
        return 0;
    }*/
    void* temp = vec->data[i];
    vec->capasity--;
    while (i < vec->capasity) {
        vec->data[i] = vec->data[i+1];
        i++;
    }
    vec->data[vec->capasity] = 0;
	return temp;
}

void* vector_pop_back(vector* vec)
{
    return vector_remove(vec, vec->capasity - 1);
}

void* vector_pop_front(vector* vec)
{
    return vector_remove(vec, 0);
}

void* vector_get(vector* vec, int i)
{
    if (!vec || vec->capasity < 0 || i > vec->capasity - 1) {
        return 0;
    }
    return vec->data[i];
}

void* vector_first(vector* vec)
{
    return vector_get(vec, 0);
}

void* vector_last(vector* vec)
{
    return vector_get(vec, vec->capasity - 1);
}

void vector_sort(vector* vec, comparator_func comparator, sort_type st)
{
    if (!vec || vec->capasity < 2) {
        return;
    }
    void* temp;
    int location;

    for (int i = 0; i < vec->capasity; i++) {
        temp = vector_get(vec, i);
        location = i - 1;
        while(location >= 0 && (((st == MIN_TO_MAX) && (comparator(vec->data[location], temp) < 0)) || ((st == MAX_TO_MIN) && (comparator(vec->data[location], temp) > 0)))) {
            vec->data[location + 1] = vec->data[location];
            location--;
        }
        vec->data[location + 1] = temp;
    }
}
