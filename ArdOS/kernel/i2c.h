/*
 * i2c.h
 *
 * Created: 04.12.2019 12:38:17
 *  Author: ITApelsin
 */ 


#ifndef I2C_H_
#define I2C_H_

#include "defines.h"
#include <stdint.h>

#ifndef F_CPU
#error "CPU frequency is not defined."
#endif

typedef enum {WRITE = 0, READ = 1} access;

void i2c_init();
// send start condition
void i2c_start();
// send stop condition
void i2c_stop();
// start data transmit
void i2c_begin(uint8_t address, access mode);
void i2c_sendByte(uint8_t value);
void i2c_sendByteTo(uint8_t value, uint8_t address);

#endif /* I2C_H_ */