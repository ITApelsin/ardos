/*
 * adc.c
 *
 * Created: 05.12.2019 12:48:20
 *  Author: ITApelsin
 */ 
#include "adc.h"
#include <avr/io.h>

#define DEFAULT _BV(REFS0)

void adc_init()
{	
	ADMUX = (1 << REFS1) |(1<<REFS0); // 2.56V max
	ADCSRA = (1<<ADEN) |(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); // divisor = 128. 16MHz / 128 = 125kHz	
}

int analogRead(a_pin pin)
{
	ADMUX = (ADMUX & 0xF0) | (pin & 0x0F) ;
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
	return ADC;
}