/*
 * i2c.c
 *
 * Created: 04.12.2019 12:50:07
 *  Author: ITApelsin
 */ 
#include "i2c.h"
#include <util/delay.h>
#include <avr/io.h>

#define TWI_STATUS (TWSR & 0xF8)	

void i2c_init()
{
	TWBR = 0x48;
}

void i2c_start()
{
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);	
	while (!(TWCR & _BV(TWINT)));
}

void i2c_stop()
{
	TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);	
}

void i2c_begin(uint8_t address, access mode)
{
	i2c_start();
	i2c_sendByte(address << 1 | (mode & 0b1));
}

void i2c_sendByte(uint8_t value) 
{
	TWDR = value;
	TWCR = _BV(TWINT) | _BV(TWEN);	
	while (!(TWCR & _BV(TWINT)));
}

void i2c_sendByteTo(uint8_t value, uint8_t address)
{
	i2c_begin(address, WRITE);
	i2c_sendByte(value);
	i2c_stop();
}