/*
 * func_flag.h
 *
 * Created: 15.11.2019 1:11:31
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_FUNC_FLAG_H_
#define ARDOS_FUNC_FLAG_H_

#define F_CPU 16000000UL

#define __ARDOS_NORETURN__ __attribute__((noreturn))
#define __ARDOS_NAKED__ __attribute__((naked))
#define __ARDOS_USED__ __attribute__((used))

// count of general purposes registers
#define ARDOS_GPR_COUNT 32

#define ARDOS_PC_SIZE 2

#ifdef __AVR_HAVE_JMP_CALL__
#	define ARDOS_CALL "call "
#	define ARDOS_JUMP "jmp "
#else //__AVR_HAVE_JMP_CALL__
#	define ARDOS_CALL "rcall "
#	define ARDOS_JUMP "rjmp "
#endif //__AVR_HAVE_JMP_CALL_


#endif /* ARDOS_FUNC_FLAG_H_ */