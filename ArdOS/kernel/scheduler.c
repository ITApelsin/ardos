/*
 * scheduler.c
 *
 * Created: 23.11.2019 18:37:59
 *  Author: ITApelsin
 */ 
#include "defines.h"
#include "scheduler.h"
#include "timer.h"
#include <vector.h>
#include <stdlib.h>
#include <util/delay.h>
#include "io.h"

typedef struct 
{
	p_func func;
	priority_t priority;
	unsigned int stack_size;
	
	unsigned long int start_time;
	unsigned long int delay ;
	char repeat;
} sch_t;

volatile vector* schedulled_tasks = 0;
volatile vector* active_processes = 0;

pH* scheduler_process = 0;
pH* nop_process = 0;

signed char process_comparator(void* v1, void* v2)
{
	pH* ph1 = (pH*) v1;
	pH* ph2 = (pH*) v2;
	if (ph1->status == ZOMBIE) {
		return -1;
	} else if (ph2->status == ZOMBIE) {
		return 1;
	} else {
		return ph1->priority == ph2->priority ? 0 : ph1->priority < ph2->priority ? -1 : 1;		
	}
}

#pragma GCC push_options
#pragma GCC optimize ("O0")
__ARDOS_USED__ int scheduler_func(void* args)
{
	while (1) {
		if (last_process) {
			if (last_process->status == ZOMBIE) 
			{
				ardos_process_free(last_process);
			} else {
				vector_push(active_processes, last_process);
			}
		}
		int tasks_count = schedulled_tasks->capasity;
		for (int i = 0; i < tasks_count; i++) 
		{
			sch_t* task = (sch_t*) vector_pop_front(schedulled_tasks);
			if (task->start_time + task->delay <= t_millis) 
			{
				pH* process = ardos_create_process(task->func, task->priority, task->stack_size);
				ardos_run_process(process, 0);
				if (task->repeat) 
				{
					task->start_time = t_millis;
					vector_push(schedulled_tasks, task);
				} else {
					ardos_free(task);	
				}
			} else {
				vector_push(schedulled_tasks, task);
			}
		}
		
		vector_sort(active_processes, process_comparator, MAX_TO_MIN);
		volatile pH* next_process = (pH*) vector_pop_front(active_processes);
		
		if (next_process) {
			ardos_select_new_process(next_process);
			ardos_save_context_and_switch_process();
		}
	}
	return 0;
}
#pragma GCC pop_options

__ARDOS_USED__ int nop_process_func(void* args)
{
	sei();
	while(1) 
	{
		_ardos_pin_xor(PORTB, PB4);
		_delay_ms(100);
	}
	cli();
	return 0;
}

void ardos_scheduler_init()
{	
	active_processes = vector_create(5);
	schedulled_tasks = vector_create(5);
	scheduler_process = ardos_create_process(scheduler_func, NORMAL, 50);
	nop_process = ardos_create_process(nop_process_func, LOW, 20);
	ardos_run_process(nop_process, 0);
	_ardos_pin_output(DDRB, DDB4);
	_ardos_pin_low(PORTB, PB4);
}

void ardos_run_process(pH* process, void* args) 
{
	process->args = args;
	vector_push(active_processes, process);
}

void ardos_schedulle_task(p_func func, priority_t priority, unsigned int stack_size, unsigned long int delay, char repeat)
{
	sch_t* task = (sch_t*) ardos_malloc(sizeof(sch_t));
	task->func = func;
	task->priority = priority;
	task->stack_size = stack_size;
	task->stack_size = t_millis;
	task->delay = delay;
	task->repeat = repeat;
	vector_push(schedulled_tasks, task);
}

void __ardos_set_scheduler_next__() 
{
	ardos_select_new_process(scheduler_process);
}