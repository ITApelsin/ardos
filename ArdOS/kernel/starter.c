/*
 * starter.c
 *
 * Created: 02.12.2019 2:05:02
 *  Author: ITApelsin
 */ 
#include "../ardos.h"

__ARDOS_NORETURN__ void ardos_init(init_func onInit)
{
	ardos_malloc_init();
	i2c_init();
	adc_init();
	t_init();
	ardos_scheduler_init();
	onInit();
	ardos_select_new_process(scheduler_process);
	ardos_switch_process();
}