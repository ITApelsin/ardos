
/*
 * Assembler1.S
 *
 * Created: 21.11.2019 1:58:44
 *  Author: ITApelsin
 */ 
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f

.global __ardos_save_context_and_switch_process__
.global __ardos_switch_process__

.extern current_process
.extern next_process
.extern __ardos_set_new_process__

__ardos_save_context_and_switch_process__:
	push r31
	in r31, __SREG__
	cli
	push r31

	push r0
	push r1
	clr r1
	push r2
	push r3
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	push r16
	push r17
	push r18
	push r19
	push r20
	push r21
	push r22
	push r23
	push r24
	push r25
	push r26
	push r27
	push r28
	push r29
	push r30

	in r28, __SP_L__
	in r29, __SP_H__
	lds r30, current_process
	lds r31, current_process + 1
	std Z + 1, r29
	std Z + 0, r28

__ardos_switch_process__:
	cli

	pop r28
	pop r29

	call __ardos_set_new_process__

	push r29
	push r28

	clr r29
	clr r28

	lds r30, current_process
	lds r31, current_process + 1

	ld r28, Z+
	ld r29, Z

	out __SP_L__, r28
	out __SP_H__, r29

	pop r30
	pop r29
	pop r28
	pop r27
	pop r26
	pop r25
	pop r24
	pop r23
	pop r22
	pop r21
	pop r20
	pop r19
	pop r18
	pop r17
	pop r16
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0

	pop r31
	out __SREG__, r31
	pop r31 

	ret