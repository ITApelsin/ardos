/*
 * ardos_malloc.c
 *
 * Created: 30.11.2019 14:54:01
 *  Author: ITApelsin
 */ 
#include <stdint.h>
#include "ardos_malloc.h"

typedef struct 
{
	char isAllocated;
	mem_size size;
} control_block;

#define HEAP_SIZE 1024
#define cb_size sizeof(control_block)

char isInit = 0;
uint8_t hB[HEAP_SIZE + cb_size] = {0};

void ardos_malloc_init()
{
	control_block* cb = (control_block*) hB;
	cb->isAllocated = 0;
	cb->size = HEAP_SIZE;
	isInit = 1;
}

mem_size block_size(control_block* cb) 
{
	return cb->size + cb_size;
}

void* split_and_alloc(void* hP, mem_size req_size) 
{
	control_block *cb = (control_block*) hP;
	control_block *newCb = (control_block*) (hP + cb_size + req_size);
	
	newCb->isAllocated = 0;
	newCb->size = cb->size - req_size - cb_size;
	
	cb->isAllocated = 1;
	cb->size = req_size;
	return hP + cb_size;
}

#pragma GCC push_options
#pragma GCC optimize ("O0")
void* find_available_block(mem_size req_size) 
{
	for (void* hP = hB; hP < hB + HEAP_SIZE + cb_size;) 
	{
		volatile control_block *cb = (control_block*)hP;
		if (cb->isAllocated || cb->size < req_size + cb_size) 
		{
			hP += block_size(cb);
		} else {
			return hP;
		}
	}
	return 0;	
}
#pragma GCC pop_options

void mem_merge() 
{
	for (void *cb = hB; cb + block_size(cb) < hB + HEAP_SIZE;) 
	{
		control_block *currentCb = (control_block*) cb;
		control_block *nextCb = (control_block*) (cb + block_size(currentCb));
		if (currentCb->isAllocated || nextCb->isAllocated) 
		{
			cb = nextCb;
		} else {
			currentCb->size = currentCb->size + nextCb->size + cb_size;
		}
	}	
}

void* ardos_malloc(mem_size req_size) 
{
	if (!isInit) {
		return 0;
	}
	void* hP = find_available_block(req_size);
	if (hP) {
		return split_and_alloc(hP, req_size);
	}
	 return 0;
}

void ardos_free(void* memP) 
{
	if (memP) {
		control_block *cb = (control_block*) (memP - cb_size);
		cb->isAllocated = 0;	
	}
	mem_merge();
}