/*
 * starter.h
 *
 * Created: 02.12.2019 2:02:33
 *  Author: ITApelsin
 */ 


#ifndef STARTER_H_
#define STARTER_H_

#include "defines.h"

typedef void (*init_func)();

__ARDOS_NORETURN__ void ardos_init(init_func onInit);

#endif /* STARTER_H_ */