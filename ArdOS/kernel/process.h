/*
 * process.h
 *
 * Created: 15.11.2019 0:23:38
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_PROCESS_H_
#define ARDOS_PROCESS_H_

#include "defines.h"
#include <util/atomic.h>
#include <stdint.h> 

#define AVR_INTERRUPT_BIT 7

typedef uint16_t sp_t;
typedef uint16_t pid_t;
typedef int (*p_func)(void* args);

typedef enum {CRITICAL = 0, HIGHT = 1, NORMAL = 2, LOW = 3} priority_t;
typedef enum {RUNNING = 0, STOPPED = 1, ZOMBIE = 3} pstatus_t;

typedef struct
{
	sp_t sp;
	priority_t priority; 
	pstatus_t status;
	void* args;
	p_func func;
	
	unsigned int stack_s;
	uint8_t *stack;
	int result;		
} pH;

extern volatile pH* last_process;

pH* ardos_create_process(p_func func, priority_t priority, unsigned int stack_s);

pH* ardos_current_process();
pH* ardos_last_process();

void ardos_process_free(pH* processH);

void ardos_select_new_process(pH* processH);

#define ardos_save_context_and_switch_process() __asm__ __volatile__ (ARDOS_CALL "__ardos_save_context_and_switch_process__" ::: "memory")
#define ardos_switch_process() __asm__ __volatile__ (ARDOS_JUMP "__ardos_switch_process__" ::: "memory")

#endif /* ARDOS_PROCESS_H_ */