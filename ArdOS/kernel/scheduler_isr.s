
/*
 * timer_isr.S
 *
 * Created: 25.11.2019 0:22:59
 *  Author: ITApelsin
 */ 
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f

.global __ardos_scheduler_isr__

.extern current_process
.extern __ardos_set_scheduler_next__
.extern __ardos_switch_process__
.extern __timer_ticks_inc__

__ardos_scheduler_isr__:
	push r31
	in r31, __SREG__
	cli
	ori 31, (1 << 7)
	push r31

	push r0
	push r1
	clr r1
	push r2
	push r3
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	push r16
	push r17
	push r18
	push r19
	push r20
	push r21
	push r22
	push r23
	push r24
	push r25
	push r26
	push r27
	push r28
	push r29
	push r30

	in r28, __SP_L__
	in r29, __SP_H__
	lds r30, current_process
	lds r31, current_process + 1
	std Z + 1, r29
	std Z + 0, r28
set_scheduler_next:
	pop r28
	pop r29

	call __ardos_set_scheduler_next__

	push r29
	push r28

	pop r28
	pop r29

	call __timer_ticks_inc__

	push r29
	push r28

	jmp __ardos_switch_process__