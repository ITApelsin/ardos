/*
 * process.c
 *
 * Created: 15.11.2019 1:31:01
 *  Author: ITApelsin
 */
#include <stdlib.h>
#include "process.h"
#include "scheduler.h"
#include "ardos_malloc.h"

// gpr_size + sreg_size + pc_size* 2
#define MINIVAL_CONTEXT_SIZE sizeof(uint8_t) * ARDOS_GPR_COUNT + sizeof(uint8_t) + sizeof(uint8_t) * ARDOS_PC_SIZE*2

volatile pH* last_process = 0;
volatile pH* current_process = 0;
volatile pH* next_process = 0;

void process_func() {
	ATOMIC_BLOCK(ATOMIC_FORCEON) 
	{
		current_process->status = RUNNING;
		current_process->result = current_process->func(current_process->args);
		current_process->status = ZOMBIE;
		ardos_select_new_process(scheduler_process);
		ardos_save_context_and_switch_process();
	}
}

uint8_t* prepare_context(uint8_t* stack_bottom)
{
	const void* address = (void*)process_func;
	*--stack_bottom = (uint8_t) ((uint16_t) address);
	*--stack_bottom = (uint8_t) (((uint16_t) address) >> 8);
	
	--stack_bottom; // r31
	*--stack_bottom = 0; // sreg
	stack_bottom -= (ARDOS_GPR_COUNT - 1);
	return --stack_bottom;
}

pH* ardos_create_process(p_func func, priority_t priority, unsigned int stack_s)
{
	pH *process_h = ardos_malloc(sizeof(pH));
	process_h->func = func;
	process_h->priority = priority;
	process_h->stack_s = stack_s * sizeof(uint8_t) + MINIVAL_CONTEXT_SIZE;
	process_h->stack = ardos_malloc(process_h->stack_s);
	process_h->status = STOPPED;
	process_h->sp = (sp_t) prepare_context(process_h->stack + process_h->stack_s);
	return process_h;
}

pH* ardos_current_process()
{
	return current_process;
}

void ardos_process_free(pH* processH) 
{
	ardos_free(processH->stack);
	if (processH->args) {
		ardos_free(processH->args);
	}
	processH->stack = 0;
	processH->args = 0;
	ardos_free(processH);
}

void ardos_select_new_process(pH* processH) {
	next_process = processH;
}

pH* ardos_last_process()
{
	return last_process;
}

void __ardos_set_new_process__() 
{
	last_process = current_process;
	current_process = next_process;
	next_process = 0;
}