/*
 * adc.h
 *
 * Created: 05.12.2019 12:46:56
 *  Author: ITApelsin
 */ 


#ifndef ADC_H_
#define ADC_H_

typedef enum {A0=0, A1 = 1, A2 = 2, A3 = 3, A4 = 4, A5 = 5} a_pin;

void adc_init();

int analogRead(a_pin pin);

#endif /* ADC_H_ */