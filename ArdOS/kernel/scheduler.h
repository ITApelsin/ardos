/*
 * scheduler.h
 *
 * Created: 23.11.2019 18:21:00
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_SCHEDULER_H_
#define ARDOS_SCHEDULER_H_

#include "process.h"

extern pH* scheduler_process;

void ardos_scheduler_init();

void ardos_run_process(pH* process, void* args);

void ardos_schedulle_task(p_func func, priority_t priority, unsigned int stack_size, unsigned long int delay, char repeat);

#endif /* ARDOS_SCHEDULER_H_ */