/*
 * timer.h
 *
 * Created: 19.10.2019 12:27:42
 *  Author: ITApelsin
 */ 


#ifndef TIMER_H_
#define TIMER_H_

void t_init();

extern volatile unsigned long int t_millis; 

#endif /* TIMER_H_ */