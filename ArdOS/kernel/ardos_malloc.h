/*
 * ardos_malloc.h
 *
 * Created: 30.11.2019 14:51:36
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_MALLOC_H_
#define ARDOS_MALLOC_H_

typedef unsigned int mem_size;

void ardos_malloc_init();

void* ardos_malloc(mem_size);

void ardos_free(void* mem_size);

#endif /* ARDOS_MALLOC_H_ */