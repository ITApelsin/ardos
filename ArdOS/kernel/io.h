/*
 * port.h
 *
 * Created: 10.10.2019 19:46:22
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_IO_H_
#define ARDOS_IO_H_

#include <avr/io.h>

#define digitalRead(PIN, BIT) PIN & _BV(BIT)

#define _ardos_pin_input(DDR, PIN) DDR &= ~_BV(PIN) 
#define _ardos_pin_output(DDR, PIN) DDR |= _BV(PIN)
#define _ardos_port_config(DDR, CONFIG) DDR = CONFIG

#define _ardos_pin_low(PORT, PIN) PORT &= ~_BV(PIN)
#define _ardos_pin_high(PORT, PIN) PORT |= _BV(PIN)
#define _ardos_pin_xor(PORT, PIN) PORT ^=_BV(PIN)

#endif /* ARDOS_IO_H_ */