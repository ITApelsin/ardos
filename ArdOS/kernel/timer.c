/*
 * timer.c
 *
 * Created: 19.10.2019 12:28:03
 *  Author: ITApelsin
 */ 
#include <avr/io.h>
#include "timer.h"
#include "scheduler.h"

#define AVR_TIMER1_FD64 _BV(CS11)|_BV(CS10)
#define AVR_TIMER1_CTC_MODE _BV(WGM12)

volatile unsigned long int t_millis;

void t_init() 
{
	t_millis = 0;
	TCCR1A = 0;
	TCCR1B = AVR_TIMER1_CTC_MODE | AVR_TIMER1_FD64;
	OCR1A = 250;
	TIMSK1 |= _BV(OCIE1A);
}

void __timer_ticks_inc__() 
{
	t_millis += 1;	
}

ISR(TIMER1_COMPA_vect, ISR_NAKED)
{
	 __asm__ __volatile__ (ARDOS_JUMP "__ardos_scheduler_isr__" ::: "memory");
}