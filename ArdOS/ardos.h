/*
 * ardos.h
 *
 * Created: 02.12.2019 1:33:59
 *  Author: ITApelsin
 */ 


#ifndef ARDOS_H_
#define ARDOS_H_

#include "kernel/ardos_malloc.h"
#include "kernel/defines.h"
#include "kernel/io.h"
#include "kernel/process.h"
#include "kernel/scheduler.h"
#include "kernel/timer.h"
#include "kernel/starter.h"
#include "kernel/i2c.h"
#include "kernel/adc.h"

#endif /* ARDOS_H_ */